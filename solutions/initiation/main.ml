(*
  * UTILS * 
  Ce sont des petites fonctions utiles pour tester les exercices
  (exécuter une fonction numérique, tester avec message d'erreur...)
*)

(* Permet de tester une fonction numérique avec une entrée utilisateur *)
let num_transformation f =
  let () = print_string "n : " in
  let n = read_int () in
  print_endline (string_of_int (f n))

(* Permet de faire des assertions avec un message d'erreur en plus *)
let assert_message cond message = assert (if not cond then print_endline message; cond)

(* Permet d'afficher une liste simplement, pour faire des tests par exemple *)
let rec print_list = function
| [] -> print_endline ""
| t::q -> print_int t; print_string " "; print_list q

(* EXERCICES *)

let helloworld () = print_endline "Hello, World!"

let hello () =
  let () = print_string "Ton nom ? " in
  let name = read_line () in
  print_endline ("Hello, " ^ name ^ "!")

let hello_alice_and_bob () =
  let () = print_string "Ton nom ? " in
  let name = read_line () in
  if name = "Alice" || name = "Bob" then print_endline ("Hello, " ^ name ^ "!")
  else print_endline "Hello, étranger !"

let hello_alice_and_bob_pattern_matching () =
  let () = print_string "Ton nom ? " in
  let name = read_line () in
  match name with
  | "Alice" | "Bob" -> print_endline ("Hello, " ^ name ^ "!")
  | _ -> print_endline "Hello, étranger !"

let rec sum_until = function
  | 0 -> 0
  | n -> n + sum_until (n - 1)

let sum_until_not_dumb n = n * (n + 1) / 2

let rec sum_three_and_five_multiples_until = function
  | 0 -> 0
  | n when n mod 3 = 0 || n mod 5 = 0 ->
      n + sum_three_and_five_multiples_until (n - 1)
  | n -> sum_three_and_five_multiples_until (n - 1)

let rec fact = function
  | 0 -> 1
  | n -> n * fact (n - 1)

let rec my_max = function
  | [] -> failwith "liste vide"
  | [ x ] -> x
  | t :: q ->
      let m = my_max q in
      if t > m then t else m

let rec appartient x l =
  match l with
  | [] -> false
  | t :: q -> t = x || appartient x q

let rec impairs l =
  match l with
  | [] -> []
  | t :: q -> t :: (impairs (impairs q))

let rec total = function
  | [] -> 0
  | t :: q -> t + total q

let rec total_aux res l = match l with
  | [] -> res
  | t :: q -> total_aux (res + t) q

let total_terminale l = total_aux 0 l

let rec reversed_aux res = function
  | [] -> res
  | t :: q -> reversed_aux q (t::res)

let reversed l = reversed_aux [] l

let palindrome l = l = reversed l

let rec transforme_aux f l res =
  match l with
  | [] -> res
  | t :: q -> transforme_aux f q (f t::res)

let transforme_aux f l = reversed (transforme_aux f l [])
