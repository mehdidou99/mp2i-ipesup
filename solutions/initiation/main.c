#include <stdio.h>
#include <string.h>
#include <assert.h>

void hello_world();
void hello();
void hello_alice_and_bob();
void sum_to_n();
void sum_to_n_less_dumb();
void sum_to_n_with_only_multiples_of_three_and_five();

int main() {
   // Vous pouvez compléter ici pour tester les différentes fonctions

   return 0;
}

void hello_world() {
   printf("Hello, World!\n");
}

void hello() {
   char name[20];
   printf("Ton nom: ");
   scanf("%20s", name);
   printf("Hello %s !\n", name);
}

void hello_alice_and_bob() {
   char name[20];
   printf("Ton nom : ");
   scanf("%20s", &name);
   if (strcmp(name, "Alice") == 0 || strcmp(name, "Bob") == 0) {
      printf("Hello %s !\n", name);
   } else {
      printf("Ouste, inconnu !\n");
   }
}

void sum_to_n() {
   int n;
   printf("n: ");
   scanf("%d", &n);
   int sum = 0;
   for (int i = 1; i <= n; ++i) {
      sum += i;
   }
   printf("La somme des entiers de 1 à %d est %d\n", n, sum);
}

void sum_to_n_less_dumb() {
   int n;
   printf("n: ");
   scanf("%d", &n);
   int sum = n * (n + 1) / 2;
   printf("La somme des entiers de 1 à %d est %d\n", n, sum);
}

void sum_to_n_with_only_multiples_of_three_and_five() {
   int n;
   printf("n: ");
   scanf("%d", &n);
   int sum = 0;
   for (int i = 1; i <= n; i++) {
      if (i % 3 == 0 || i % 5 == 0) {
         printf("%d; ", i);
         sum += i;
      }
   }
   printf("La somme des multiples de 3 et 5 de 1 à %d est %d\n", n, sum);
}

void max(int l[], int size) {
   int max = l[0];
   for (int i = 1; i < size; i++) {
      if (l[i] > max) {
         max = l[i];
      }
   }
   return max;
}

void reverse(int l[], int size) {
   for (int i = 0; i < size / 2; i++) {
      int tmp = l[i];
      l[i] = l[size - i - 1];
      l[size - i - 1] = tmp;
   }
}

void appartient(int x, int l[], int size) {
   for (int i = 0; i < size; i++) {
      if (l[i] == x) {
         return 1;
      }
   }
   return 0;
}

void positions_impaires(int l[], int size, int res[]) {
   int j = 0;
   for (int i = 1; i < size; i += 2) {
      res[j] = l[i];
      j++;
   }
}

int total(int l[], int size) {
   int sum = 0;
   for (int i = 0; i < size; i++) {
      sum += l[i];
   }
   return sum;
}

int palindrome(int l[], int size) {
   int i = 0;
   int j = size - 1;
   while (i < j) {
      if (l[i] != l[j]) {
         return 0;
      }
      i++;
      j--;
   }
   return 1;
}
