(* Ceci est un éditeur pour OCaml
   Entrez votre programme ici, et envoyez-le au toplevel en utilisant le
   bouton "Évaluer le code" ci-dessous ou [Ctrl-e]. *) 

   type liste = 
   | Vide
   | Noeud of int * liste 
              
 let liste_vide () = Vide 
 let test_liste_vide () =
   (* a *)
   (* b *)
   let res = liste_vide () in
   (* c *)
   assert (res = Vide)
   
 let ajout_debut x l = Noeud(x, l) 
 let test_ajout_debut_vide () =
   (* a *)
   let x = 5 in
   let l = Vide in
   (* b *)
   let res = ajout_debut x l in
   (* c *)
   assert (res = Noeud (5, Vide))
 let test_ajout_debut_nonvide () =
   (* a *)
   let x = 3 in
   let l = Noeud(2,
                 Noeud(4,
                       Noeud(5, Vide))) in
   (* b *)
   let res = ajout_debut x l in
   (* c *)
   assert (res = Noeud(3,
                       Noeud(2,
                             Noeud(4,
                                   Noeud(5, Vide))))
          )
   